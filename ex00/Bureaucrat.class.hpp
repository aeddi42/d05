/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.class.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:23:09 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/13 14:25:23 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

#include <iostream>
#include <stdexcept>

class Bureaucrat {

	public:

							Bureaucrat(std::string const & name, int const range);
							Bureaucrat(Bureaucrat const & src);
		Bureaucrat&			operator=(Bureaucrat const & rhs);
							~Bureaucrat(void);

		std::string			getName(void) const;
		int					getGrade(void) const;

		void				incGrade(void);
		void				decGrade(void);


		class GradeTooHighException : public std::exception {

			public:
				virtual const char* what() const throw();
									GradeTooHighException(void);
							virtual	~GradeTooHighException(void) throw();
									GradeTooHighException(GradeTooHighException const & src);
			GradeTooHighException&	operator=(GradeTooHighException const & rhs);
		};

		class GradeTooLowException : public std::exception {

			public :
				virtual const char* what() const throw();
									GradeTooLowException(void);
							virtual	~GradeTooLowException(void) throw();
									GradeTooLowException(GradeTooLowException const & src);
			GradeTooLowException&	operator=(GradeTooLowException const & rhs);
		};


	private:

							Bureaucrat(void);

		const std::string	_name;
		int					_grade;

};

std::ostream&	operator<<(std::ostream & out, Bureaucrat const & in);

#endif /* !BUREAUCRAT_HPP */
