/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 18:14:42 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.class.hpp"

int		main(void) {

	Bureaucrat *jacky;
	Bureaucrat *jacko;

	try {
		std::cout << "Try to create a Bureaucrat with a grade of 0:" << std::endl;
		jacky = new Bureaucrat("Jacky", 0);
		std::cout << *jacky;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to create a Bureaucrat with a grade of 151:" << std::endl;
		jacko = new Bureaucrat("Jacko", 151);
		std::cout << *jacko;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to create a Bureaucrat with a grade of 2:" << std::endl;
		jacky = new Bureaucrat("Jacky", 2);
		std::cout << *jacky;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to create a Bureaucrat with a grade of 149:" << std::endl;
		jacko = new Bureaucrat("Jacko", 149);
		std::cout << *jacko;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "====================================================" << std::endl;
	try {
		std::cout << std::endl << "Try to increment the grade of a Bureaucrat level 2:" << std::endl;
		jacky->incGrade();
		std::cout << *jacky;
		std::cout << std::endl << "Try to increment the grade of a Bureaucrat level 1:" << std::endl;
		jacky->incGrade();
		std::cout << *jacky;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to decrement the grade of a Bureaucrat level 149:" << std::endl;
		jacko->decGrade();
		std::cout << *jacko;
		std::cout << std::endl << "Try to decrement the grade of a Bureaucrat level 150:" << std::endl;
		jacko->decGrade();
		std::cout << *jacko;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	return 0;

}
