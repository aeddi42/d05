/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:13:17 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 15:36:46 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"
#include <cstdlib>
#include <ctime>

		RobotomyRequestForm::RobotomyRequestForm(std::string const & target)
		: Form("Robotomy Form", target, 72, 45) {}

		RobotomyRequestForm::~RobotomyRequestForm(void) {}

void	RobotomyRequestForm::doAction(std::string const & target) const {

	std::srand(std::time(NULL));
	std::cout << '\a' << '\a' << '\a' << "Bip Bip Bip ! ";
	if (std::rand() % 2)
		std::cout << target << " has been robotomized successfully !"<< std::endl;
	else
		std::cout << target << " robotomyzation failure !" << std::endl;
}
