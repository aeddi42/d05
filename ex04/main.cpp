/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 19:30:12 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"
#include "Form.class.hpp"
#include "PresidentialPardonForm.hpp"
#include <iostream>

void	tryToDoBureaucracy(OfficeBlock & office, std::string const & form, std::string const & target) {

	try {
		office.doBureaucracy(form, target);
		std::cout << "That's how we do good bureaucracy !" << std::endl;
	}
	catch(Form::FormException & e) {
		std::cout <<  e.what() << std::endl;
		std::cout << "Impossible to do good bureaucracy with incompetents bureaucrats !" << std::endl;
	}
	catch(Intern::UnknowForm & e) {
		std::cout <<  e.what() << std::endl;
		std::cout << "How can we do good bureaucracy with such **** directives ?" << std::endl;
	}
	catch(OfficeBlock::IncompleteOfficeBlockException & e) {
		std::cout <<  e.what() << std::endl;
		std::cout << "Impossible to do good bureaucracy with an incomplete office staff !" << std::endl;
	}
	catch(std::exception & e) {
		std::cout << "Unknow error... but we can't do good bureaucracy !" << std::endl;
	}
	std::cout << std::endl;
}

int		main(void) {

	Intern		*jack;
	Bureaucrat	*jacky;
	Bureaucrat	*jacko;
	OfficeBlock	*office;

	try {
		jacko = new Bureaucrat("Jacko", 151);;
	}
	catch(Bureaucrat::BureaucratException & e) {
		std::cout <<  e.what() << std::endl;
		std::cout << "Impossible to do good bureaucracy if RH doesn't hire good bureaucrat !" << std::endl;
		std::cout << std::endl;
	}

	jack = new Intern();
	jacky = new Bureaucrat("Jacky", 42);;
	jacko = new Bureaucrat("Jacko", 84);;

	office = new OfficeBlock();

	tryToDoBureaucracy(*office, "shrubbery creation", "Forest");
	office->setSign(jacky);
	office->setExec(jacko);
	office->setCreate(jack);

	try {
		PresidentialPardonForm test("Test");
		jacky->executeForm(test);
	}
	catch(Form::NotSignedException & e) {
		std::cout <<  e.what() << std::endl;
		std::cout << "Impossible to do good bureaucracy with such **** organization !" << std::endl;
		std::cout << std::endl;
	}

	tryToDoBureaucracy(*office, "shrubbery creation", "Forest");
	tryToDoBureaucracy(*office, "robotomy request", "Bender");
	tryToDoBureaucracy(*office, "presidential pardon", "Rambo");
	tryToDoBureaucracy(*office, "NOPE", "EPON");

	delete jack;
	delete jacky;
	delete jacko;
	delete office;

	return 0;
}
