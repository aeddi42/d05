/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 19:14:58 by plastic           #+#    #+#             */
/*   Updated: 2015/04/14 18:28:19 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"
#include "Form.class.hpp"

		OfficeBlock::OfficeBlock(void)
		: _sign(NULL), _exec(NULL), _create(NULL) {}

		OfficeBlock::OfficeBlock(Bureaucrat *sign, Bureaucrat *exec, Intern *create)
		: _sign(sign), _exec(exec), _create(create) {}

		OfficeBlock::~OfficeBlock(void) {}

void	OfficeBlock::setSign(Bureaucrat *sign) { this->_sign = sign; }

void	OfficeBlock::setExec(Bureaucrat *exec) { this->_exec = exec; }

void	OfficeBlock::setCreate(Intern *create) { this->_create = create; }

void	OfficeBlock::doBureaucracy(std::string const & form, std::string const & name) {

	Form*	tmp;
	
	tmp = NULL;
	if (!this->_sign || !this->_exec || !this->_create)
		throw IncompleteOfficeBlockException();
	else {
		try {
			tmp = this->_create->makeForm(form, name);
			this->_sign->signForm(*tmp);
			this->_exec->executeForm(*tmp);
			delete tmp;
			return;
		}
		catch (...) {
			if (tmp)
				delete tmp;
			throw;
		}
	}
}

const char * OfficeBlock::IncompleteOfficeBlockException::what() const throw() {

	return("This office block is not complete !");
}

OfficeBlock::IncompleteOfficeBlockException::IncompleteOfficeBlockException(void) {}

OfficeBlock::IncompleteOfficeBlockException::~IncompleteOfficeBlockException(void) throw() {}
