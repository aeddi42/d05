/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.class.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:23:09 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 17:26:48 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

#include <iostream>
#include <stdexcept>
#include "Form.class.hpp"

class Bureaucrat {

	public:

							Bureaucrat(std::string const & name, int const range);
							Bureaucrat(Bureaucrat const & src);
		Bureaucrat&			operator=(Bureaucrat const & rhs);
							~Bureaucrat(void);

		std::string			getName(void) const;
		int					getGrade(void) const;

		void				incGrade(void);
		void				decGrade(void);

		void				signForm(Form& tosign) const;
		void				executeForm(Form const & form) const;

		class BureaucratException : public std::exception {

			public:
				virtual const char* what() const throw();
									BureaucratException(void);
									BureaucratException(BureaucratException const & src);
			BureaucratException&	operator=(BureaucratException const & rhs);
							virtual	~BureaucratException(void) throw();
		};

		class GradeTooHighException : public BureaucratException {

			public:
				virtual const char* what() const throw();
									GradeTooHighException(void);
									GradeTooHighException(GradeTooHighException const & src);
			GradeTooHighException&	operator=(GradeTooHighException const & rhs);
							virtual	~GradeTooHighException(void) throw();
		};

		class GradeTooLowException : public BureaucratException {

			public :
				virtual const char* what() const throw();
									GradeTooLowException(void);
									GradeTooLowException(GradeTooLowException const & src);
			GradeTooLowException&	operator=(GradeTooLowException const & rhs);
							virtual	~GradeTooLowException(void) throw();
		};


	private:

							Bureaucrat(void);

		const std::string	_name;
		int					_grade;

};

std::ostream&	operator<<(std::ostream & out, Bureaucrat const & in);

#endif /* !BUREAUCRAT_HPP */
