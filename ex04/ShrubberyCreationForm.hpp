/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:12:57 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 17:50:05 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include "Form.class.hpp"

class ShrubberyCreationForm : public Form {

	public:
		ShrubberyCreationForm(std::string const & target);
		~ShrubberyCreationForm(void);

		void	doAction(std::string const & target) const;

	private:
		ShrubberyCreationForm(void);
		ShrubberyCreationForm(ShrubberyCreationForm const & src);
		ShrubberyCreationForm& operator=(ShrubberyCreationForm const & rhs);

};

#endif /* !SHRUBBERYCREATIONFORM_HPP */
