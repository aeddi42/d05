/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 18:17:29 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.class.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>

int		main(void) {

	Bureaucrat	*jacky;
	Bureaucrat	*jacko;
	Form		*shru;
	Form		*pres;

	jacky = new Bureaucrat("Jacky", 1);
	jacko = new Bureaucrat("Jacko", 42);

	pres = new PresidentialPardonForm("Rambo");
	shru = new ShrubberyCreationForm("Forest");

	std::cout << "Try to execute an unsigned Form 137 with a Bureaucrat 42:" << std::endl;
	jacko->executeForm(*shru);
	std::cout << std::endl;

	jacky->signForm(*shru);
	std::cout << "Try to execute a signed Form 137 with a Bureaucrat 42:" << std::endl;
	jacko->executeForm(*shru);
	std::cout << std::endl;

	jacky->signForm(*pres);
	std::cout << "Try to execute a signed Form 5 with a Bureaucrat 42:" << std::endl;
	jacko->executeForm(*pres);

	delete jacko;
	delete jacky;
	delete shru;
	delete pres;

	return 0;

}
