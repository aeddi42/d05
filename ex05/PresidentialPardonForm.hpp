/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:13:37 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 17:50:11 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP

# include "Form.class.hpp"

class PresidentialPardonForm : public Form {

	public:
		PresidentialPardonForm(std::string const & target);
		~PresidentialPardonForm(void);

		void	doAction(std::string const & target) const;

	private:
		PresidentialPardonForm(void);
		PresidentialPardonForm(PresidentialPardonForm const & src);
		PresidentialPardonForm& operator=(PresidentialPardonForm const & rhs);

};

#endif /* !PRESIDENTIALPARDONFORM_HPP */
