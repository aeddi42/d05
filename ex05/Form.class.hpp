/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.class.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 17:18:48 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 13:58:30 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

#include <iostream>
#include <stdexcept>

class Bureaucrat;

class Form {

	public:

						Form(std::string const & name, std::string const & target, int grade, int grade_ex);
virtual					~Form(void);

		std::string const &		getName(void) const;
		std::string const &		getTarget(void) const;
		bool					getSigned(void) const;
		int						getGradeMin(void) const;
		int						getGradeMinEx(void) const;

		void			beSigned(Bureaucrat const & auth);
		void			execute(Bureaucrat const & executor) const;

virtual void			doAction(std::string const & target) const = 0;

		class FormException : public std::exception { 

			public:
				virtual const char* what() const throw();
									FormException(void);
									FormException(FormException const & src);
					FormException&	operator=(FormException const & rhs);
							virtual	~FormException(void) throw();
		};

		class GradeTooHighException : public FormException {

			public:
				virtual const char* what() const throw();
									GradeTooHighException(void);
									GradeTooHighException(GradeTooHighException const & src);
			GradeTooHighException&	operator=(GradeTooHighException const & rhs);
							virtual	~GradeTooHighException(void) throw();
		};

		class GradeTooLowException : public FormException {

			public:
				virtual const char* what() const throw();
									GradeTooLowException(void);
									GradeTooLowException(GradeTooLowException const & src);
			GradeTooLowException&	operator=(GradeTooLowException const & rhs);
							virtual	~GradeTooLowException(void) throw();
		};

		class SignGradeTooLowException : public FormException {

			public:
				virtual const char* what() const throw();
									SignGradeTooLowException(void);
									SignGradeTooLowException(SignGradeTooLowException const & src);
			SignGradeTooLowException&	operator=(SignGradeTooLowException const & rhs);
							virtual	~SignGradeTooLowException(void) throw();
		};

		class ExecGradeTooLowException : public FormException {

			public:
				virtual const char* what() const throw();
									ExecGradeTooLowException(void);
									ExecGradeTooLowException(ExecGradeTooLowException const & src);
			ExecGradeTooLowException&	operator=(ExecGradeTooLowException const & rhs);
							virtual	~ExecGradeTooLowException(void) throw();
		};

		class NotSignedException : public FormException {

			public:
				virtual const char* what() const throw();
									NotSignedException(void);
									NotSignedException(NotSignedException const & src);
			NotSignedException&	operator=(NotSignedException const & rhs);
							virtual	~NotSignedException(void) throw();
		};


	private:

						Form(void);
						Form(Form const & src);
		Form&			operator=(Form const & rhs);

		const std::string	_name;
		const std::string	_target;
		bool				_signed;
		const int			_min_grade;
		const int			_min_grade_ex;

};

std::ostream&	operator<<(std::ostream & out, Form const & in);

#endif /* !FORM_HPP */
