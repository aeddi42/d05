/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 19:14:58 by plastic           #+#    #+#             */
/*   Updated: 2015/04/14 18:58:05 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OFFICEBLOCK_HPP
# define OFFICEBLOCK_HPP

# include "Bureaucrat.class.hpp"
# include "Intern.hpp"

class OfficeBlock {

	public:
		OfficeBlock(void);
		OfficeBlock(Bureaucrat *sign, Bureaucrat *exec, Intern *create);
		~OfficeBlock(void);

		void	setSign(Bureaucrat *sign);
		void	setExec(Bureaucrat *exec);
		void	setCreate(Intern *create);

		Bureaucrat	*getSign(void);
		Bureaucrat	*getExec(void);
		Intern		*getCreate(void);

		void	doBureaucracy(std::string const & form, std::string const & name);

		class IncompleteOfficeBlockException : public std::exception {

			public:
				virtual const char* what() const throw();
									IncompleteOfficeBlockException(void);
									IncompleteOfficeBlockException(IncompleteOfficeBlockException const & src);
			IncompleteOfficeBlockException&	operator=(IncompleteOfficeBlockException const & rhs);
							virtual	~IncompleteOfficeBlockException(void) throw();
		};


	private:
		OfficeBlock(OfficeBlock const & src);
		OfficeBlock& operator=(OfficeBlock const & rhs);

		Bureaucrat	*_sign;
		Bureaucrat	*_exec;
		Intern		*_create;

};

#endif /* !OFFICEBLOCK_HPP */
