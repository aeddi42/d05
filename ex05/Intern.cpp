/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 18:10:50 by plastic           #+#    #+#             */
/*   Updated: 2015/04/14 18:28:07 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

			Intern::Intern(void) {}

			Intern::~Intern(void) {}

Form*		Intern::makeForm(std::string const & form, std::string const & target) {

		Form*	ret;

		if (form == "shrubbery creation")
			ret = new ShrubberyCreationForm(target);
		else if (form == "robotomy request")
			ret = new RobotomyRequestForm(target);
		else if (form == "presidential pardon")
			ret = new PresidentialPardonForm(target);
		else
			throw Intern::UnknowForm();
		std::cout << "Intern creates " << form << std::endl;
		return ret;
}

const char * Intern::UnknowForm::what() const throw() {

	return("This kind of form is unknow.");
}

Intern::UnknowForm::UnknowForm(void) {}

Intern::UnknowForm::~UnknowForm(void) throw() {}
