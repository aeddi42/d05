/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 20:35:25 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CentralBureaucracy.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

int		main(void) {

	CentralBureaucracy		*offices;
	Bureaucrat				*tmp;
	int						i;
	int						i2;

	offices = new CentralBureaucracy;

	for (i = i2 = 0; i < 42; i2++) {
		try {
			std::srand(std::time(NULL) + i + i2);
			tmp = new Bureaucrat("Bureaucrat #" + std::to_string(i), std::rand() % 165);
			offices->feedOffice(tmp);
			i++;
		}
		catch(Bureaucrat::BureaucratException & e) {
			std::cout <<  e.what() << std::endl;
			std::cout << "Impossible to do good bureaucracy if RH doesn't hire good bureaucrat !" << std::endl;
			std::cout << std::endl;
		}
	}

	for (i = 0; i < 84; i++) {
		offices->queueUp(new std::string("Target #" + std::to_string(i)));
	}

	offices->doBureaucracy();

	delete offices;

	return 0;
}
