/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:13:37 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 18:04:42 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"
#include <iostream>

		PresidentialPardonForm::PresidentialPardonForm(std::string const & target)
		: Form("Presidential Form", target, 25, 5) {}

		PresidentialPardonForm::~PresidentialPardonForm(void) {}

void	PresidentialPardonForm::doAction(std::string const & target) const {

	std::cout << target << " has been pardoned by Zafod Beeblebrox !" << std::endl;
}
