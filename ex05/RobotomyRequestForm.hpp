/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:13:17 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 17:50:21 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "Form.class.hpp"

class RobotomyRequestForm : public Form {

	public:
		RobotomyRequestForm(std::string const & target);
		~RobotomyRequestForm(void);

		void	doAction(std::string const & target) const;

	private:
		RobotomyRequestForm(void);
		RobotomyRequestForm(RobotomyRequestForm const & src);
		RobotomyRequestForm& operator=(RobotomyRequestForm const & rhs);

};

#endif /* !ROBOTOMYREQUESTFORM_HPP */
