/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 18:10:50 by plastic           #+#    #+#             */
/*   Updated: 2015/04/14 12:25:16 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
# define INTERN_HPP

# include "Form.class.hpp"

class Intern {

	public:
		Intern(void);
		~Intern(void);

		Form*	makeForm(std::string const & form, std::string const & target);

		class UnknowForm : public std::exception {

			public:
				virtual const char* what() const throw();
									UnknowForm(void);
									UnknowForm(UnknowForm const & src);
			UnknowForm&	operator=(UnknowForm const & rhs);
							virtual	~UnknowForm(void) throw();
		};

	private:
		Intern(Intern const & src);
		Intern& operator=(Intern const & rhs);

};

#endif /* !INTERN_HPP */
