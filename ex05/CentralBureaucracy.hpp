/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CentralBureaucracy.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/14 18:42:14 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 20:12:51 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CENTRALBUREAUCRACY_HPP
# define CENTRALBUREAUCRACY_HPP

# include "OfficeBlock.hpp"

class CentralBureaucracy {

	public:
		CentralBureaucracy(void);
		~CentralBureaucracy(void);

		void	feedOffice(Bureaucrat *random);
		void	queueUp(std::string *target);
		void	doBureaucracy(void);

	private:
		CentralBureaucracy(CentralBureaucracy const & src);
		CentralBureaucracy& operator=(CentralBureaucracy const & rhs);

		OfficeBlock	_offices[20];
		int			_count;
		std::string	**_queue;

};

#endif /* !CENTRALBUREAUCRACY_HPP */
