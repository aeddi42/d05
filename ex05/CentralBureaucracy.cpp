/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CentralBureaucracy.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/14 18:42:14 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 20:34:05 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CentralBureaucracy.hpp"
#include <cstdlib>
#include <ctime>

		CentralBureaucracy::CentralBureaucracy(void) : _count(0) {}

		CentralBureaucracy::~CentralBureaucracy(void) {
	
	int	i;

	for (i = 0; i < 20; i++) {
		if (this->_offices[i].getSign())
			delete this->_offices[i].getSign();
		else if (this->_offices[i].getExec())
			delete this->_offices[i].getExec();
		else if (this->_offices[i].getCreate())
			delete this->_offices[i].getCreate();
	}
	if (this->_count)
		delete [] this->_queue;
}

void	CentralBureaucracy::feedOffice(Bureaucrat *random) {

	int	i;

	if (random) {
		for (i = 0; i < 20; i++) {
			if (!this->_offices[i].getSign()) {
				this->_offices[i].setSign(random);
				return;
			}
			else if (!this->_offices[i].getExec()) {
				this->_offices[i].setExec(random);
				this->_offices[i].setCreate(new Intern);
				return;
			}
		}
		std::cout << "Our offices is full ! Get out " << random->getName() << " !" << std::endl;
		delete random;
	}
}

void	CentralBureaucracy::queueUp(std::string *target) {

	std::string	**tmp;
	int	i;

	tmp = new std::string*[this->_count + 1];
	for (i = 0; i < this->_count; i++) {
		tmp[i] = this->_queue[i];
	}
	tmp[this->_count] = target;
	if (this->_count)
		delete [] this->_queue;
	this->_count++;
	this->_queue = tmp;
}

void	CentralBureaucracy::doBureaucracy(void) {

	int				i;
	std::string		forms[4] = {
		"shrubbery creation",
		"robotomy request",
		"presidential pardon",
		"NOPE"
	};

	for (i = 0; i < this->_count; i++) {
		std::srand(std::time(NULL) + i);
		try {
			this->_offices[i % 20].doBureaucracy(forms[std::rand() % 4], *this->_queue[i]);
			std::cout << "That's how we do good bureaucracy !" << std::endl;
		}
		catch(Form::FormException & e) {
			std::cout <<  e.what() << std::endl;
			std::cout << "Impossible to do good bureaucracy with incompetents bureaucrats !" << std::endl;
		}
		catch(Intern::UnknowForm & e) {
			std::cout <<  e.what() << std::endl;
			std::cout << "How can we do good bureaucracy with such **** directives ?" << std::endl;
		}
		catch(OfficeBlock::IncompleteOfficeBlockException & e) {
			std::cout <<  e.what() << std::endl;
			std::cout << "Impossible to do good bureaucracy with an incomplete office staff !" << std::endl;
		}
		catch(std::exception & e) {
			std::cout << "Unknow error... but we can't do good bureaucracy !" << std::endl;
		}
		delete this->_queue[i];
		std::cout << std::endl;
	}
}
