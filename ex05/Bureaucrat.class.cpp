/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.class.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:00 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 18:29:24 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.class.hpp"

				Bureaucrat::Bureaucrat(std::string const & name, int const grade) : _name(name) {
	
		if (grade < 1)
			throw Bureaucrat::GradeTooHighException();
		else if (grade > 150)
			throw Bureaucrat::GradeTooLowException();
		else
			this->_grade = grade;
}

				Bureaucrat::Bureaucrat(Bureaucrat const & src) { *this = src; }

Bureaucrat&		Bureaucrat::operator=(Bureaucrat const & rhs) {

	this->_grade = rhs._grade;

	return *this;
}

				Bureaucrat::~Bureaucrat(void) {}

std::string		Bureaucrat::getName(void) const { return this->_name; }
		
int				Bureaucrat::getGrade(void) const { return this->_grade; }

void			Bureaucrat::incGrade(void) {

	if (this->_grade <= 1)
		throw Bureaucrat::GradeTooHighException();
	else
		this->_grade--;
}

void			Bureaucrat::decGrade(void) {

	if (this->_grade >= 150)
		throw Bureaucrat::GradeTooLowException();
	else
		this->_grade++;
}

void			Bureaucrat::signForm(Form& tosign) const {

	std::string	sign;

	sign = (tosign.getSigned()) ? "Signed" : "Unsigned";
	tosign.beSigned(*this);	
	std::cout << "Bureaucrat " << this->_name << " with a grade of " << this->_grade << " signs a " << tosign.getName()
	<< " (s.grade " <<  tosign.getGradeMin() << ", ex.grade " << tosign.getGradeMinEx() <<  ")"
	<< " - targeted on " << tosign.getTarget() << " ("  << sign << ")" << std::endl;
}

void			Bureaucrat::executeForm(Form const & form) const {

	std::string	sign;

	sign = (form.getSigned()) ? "Signed" : "Unsigned";
	form.execute(*this);	
	std::cout << "Bureaucrat " << this->_name << " with a grade of " << this->_grade << " executes a " << form.getName()
	<< " (s.grade " <<  form.getGradeMin() << ", ex.grade " << form.getGradeMinEx() <<  ")"
	<< " - targeted on " << form.getTarget() << " ("  << sign << ")" << std::endl;
}

std::ostream&	operator<<(std::ostream & out, Bureaucrat const & in) {

	out << in.getName() << ", bureaucrat grade " << in.getGrade() << std::endl;

	return out;
}

const char * Bureaucrat::BureaucratException::what() const throw() {

	return NULL;
}

const char * Bureaucrat::GradeTooHighException::what() const throw() {

	return("The bureaucrat grade can't be higher than 1.");
}

const char * Bureaucrat::GradeTooLowException::what() const throw() {

	return("The bureaucrat grade can't be lower than 150.");
}

Bureaucrat::BureaucratException::BureaucratException(void) {}

Bureaucrat::BureaucratException::~BureaucratException(void) throw() {}

Bureaucrat::GradeTooHighException::GradeTooHighException(void) {}

Bureaucrat::GradeTooHighException::~GradeTooHighException(void) throw() {}

Bureaucrat::GradeTooLowException::GradeTooLowException(void) {}

Bureaucrat::GradeTooLowException::~GradeTooLowException(void) throw() {}
