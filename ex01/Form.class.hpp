/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.class.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 17:18:48 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/13 11:56:04 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

#include <iostream>
#include <stdexcept>

class Bureaucrat;

class Form {

	public:

						Form(std::string name, int grade, int grade_ex);
						~Form(void);

		std::string		getName(void) const;
		bool			getSigned(void) const;
		int				getGradeMin(void) const;
		int				getGradeMinEx(void) const;

		void			beSigned(Bureaucrat const & auth);

		class GradeTooHighException : public std::exception {

			public:
				virtual const char* what() const throw();
									GradeTooHighException(void);
									GradeTooHighException(GradeTooHighException const & src);
			GradeTooHighException&	operator=(GradeTooHighException const & rhs);
							virtual	~GradeTooHighException(void) throw();
		};

		class GradeTooLowException : public std::exception {

			public:
				virtual const char* what() const throw();
									GradeTooLowException(void);
									GradeTooLowException(GradeTooLowException const & src);
			GradeTooLowException&	operator=(GradeTooLowException const & rhs);
							virtual	~GradeTooLowException(void) throw();
		};


	private:

						Form(void);
						Form(Form const & src);
		Form&			operator=(Form const & rhs);

		const std::string	_name;
		bool				_signed;
		const int			_min_grade;
		const int			_min_grade_ex;

};

std::ostream&	operator<<(std::ostream & out, Form const & in);

#endif /* !FORM_HPP */
