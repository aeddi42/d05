/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/13 12:03:10 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.class.hpp"
#include "Form.class.hpp"
#include <iostream>

int		main(void) {

	Bureaucrat	*jacko;
	Form		*contract1;
	Form		*contract2;

	jacko = new Bureaucrat("Jacko", 42);

	try {
		std::cout << "Try to create a Form with min grades of 0 0:" << std::endl;
		contract1 = new Form("Big contrat", 0, 0);
		std::cout << *contract1;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to create a Form with min grades of 151 151:" << std::endl;
		contract2 = new Form("Small contrat", 151, 151);
		std::cout << *contract2;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout << std::endl << "Try to create a Form with min grades of 10 10:" << std::endl;
		contract1 = new Form("Big contrat", 10, 10);
		std::cout << *contract1;
		std::cout << std::endl << "Try to create a Form with min grades of 115 115:" << std::endl;
		contract2 = new Form("Small contrat", 115, 115);
		std::cout << *contract2;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "====================================================" << std::endl;
	std::cout << std::endl << "Try to sign a Form 10 10 with a Bureaucrat 42:" << std::endl;
	jacko->signForm(*contract1);
	std::cout << std::endl << "Try to sign a Form 115 115 with a Bureaucrat 42:" << std::endl;
	jacko->signForm(*contract2);

	delete jacko;
	delete contract1;
	delete contract2;

	return 0;

}
