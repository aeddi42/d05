/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 17:18:25 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/13 11:59:22 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.class.hpp"
#include "Bureaucrat.class.hpp"

			Form::Form(std::string name, int grade, int grade_ex)
			: _name(name), _signed(false), _min_grade(grade), _min_grade_ex(grade_ex) {

	if (this->_min_grade < 1)
		throw Form::GradeTooHighException();
	else if (this->_min_grade > 150)
		throw Form::GradeTooLowException();

	if (this->_min_grade_ex < 1)
		throw Form::GradeTooHighException();
	else if (this->_min_grade_ex > 150)
		throw Form::GradeTooLowException();
}

				Form::~Form(void) {}

void			Form::beSigned(Bureaucrat const & auth) {

	if (auth.getGrade() <= this->_min_grade)
		this->_signed = true;
	else
		throw Form::GradeTooLowException();
}

std::string		Form::getName(void) const { return this->_name; }

bool			Form::getSigned(void) const { return this->_signed; }

int				Form::getGradeMin(void) const { return this->_min_grade; }

int				Form::getGradeMinEx(void) const { return this->_min_grade_ex; }

std::ostream&	operator<<(std::ostream & out, Form const & in) {

	std::string	sign;

	sign = (in.getSigned()) ? "signed" : "not signed";
	out << in.getName() << " is " << sign << ". ";
	out << "Grade min to sign it: " << in.getGradeMin() << ". ";
	out << "Grade min to execute it: " << in.getGradeMinEx() << "." << std::endl;

	return out;
}

const char * 	Form::GradeTooHighException::what() const throw() {

	return("The grade is to high.");
}

const char * 	Form::GradeTooLowException::what() const throw() {

	return("The grade is to low.");
}

Form::GradeTooHighException::GradeTooHighException(void) {}

Form::GradeTooHighException::~GradeTooHighException(void) throw() {}

Form::GradeTooLowException::GradeTooLowException(void) {}

Form::GradeTooLowException::~GradeTooLowException(void) throw() {}
