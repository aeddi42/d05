/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:12:57 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 15:25:43 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include <fstream>

		ShrubberyCreationForm::ShrubberyCreationForm(std::string const & target)
		: Form("Shrubbery Form", target, 145, 137) {}

		ShrubberyCreationForm::~ShrubberyCreationForm(void) {}

void	ShrubberyCreationForm::doAction(std::string const & target) const {

	std::ofstream file((target + "_shrubbery").c_str());

	file << "            \\ /        " << std::endl;
	file << "          -->*<--      " << std::endl;
	file << "            /_\\        " << std::endl;
	file << "           /_\\_\\       " << std::endl;
	file << "          /_/_/_\\      " << std::endl;
	file << "          /_\\_\\_\\      " << std::endl;
	file << "         /_/_/_/_\\     " << std::endl;
	file << "         /_\\_\\_\\_\\     " << std::endl;
	file << "        /_/_/_/_/_\\    " << std::endl;
	file << "        /_\\_\\_\\_\\_\\    " << std::endl;
	file << "       /_/_/_/_/_/_\\   " << std::endl;
	file << "       /_\\_\\_\\_\\_\\_\\   " << std::endl;
	file << "      /_/_/_/_/_/_/_\\  " << std::endl;
	file << "           [___]       " << std::endl;

	file.close();
}
