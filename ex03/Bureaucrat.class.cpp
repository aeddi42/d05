/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.class.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:00 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 17:59:36 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.class.hpp"

				Bureaucrat::Bureaucrat(std::string const & name, int const grade) : _name(name) {
	
		if (grade < 1)
			throw Bureaucrat::GradeTooHighException();
		else if (grade > 150)
			throw Bureaucrat::GradeTooLowException();
		else
			this->_grade = grade;
}

				Bureaucrat::Bureaucrat(Bureaucrat const & src) { *this = src; }

Bureaucrat&		Bureaucrat::operator=(Bureaucrat const & rhs) {

	this->_grade = rhs._grade;

	return *this;
}

				Bureaucrat::~Bureaucrat(void) {}

std::string		Bureaucrat::getName(void) const { return this->_name; }
		
int				Bureaucrat::getGrade(void) const { return this->_grade; }

void			Bureaucrat::incGrade(void) {

	if (this->_grade <= 1)
		throw Bureaucrat::GradeTooHighException();
	else
		this->_grade--;
}

void			Bureaucrat::decGrade(void) {

	if (this->_grade >= 150)
		throw Bureaucrat::GradeTooLowException();
	else
		this->_grade++;
}

void			Bureaucrat::signForm(Form& tosign) const {

	try {
		tosign.beSigned(*this);	
		std::cout << this->_name	<< " sign " << tosign.getName() << std::endl;
	}
	catch (Form::GradeTooLowException & e) {
		std::cout << this->_name << " cannot sign " << tosign.getName() << " because " << e.what() << std::endl;
	}
}

void			Bureaucrat::executeForm(Form const & form) const {

	try {
		form.execute(*this);	
		std::cout << this->_name << " execute " << form.getName() << std::endl;
	}
	catch (Form::GradeTooLowException & e) {
		std::cout << this->_name << " cannot execute " << form.getName() << " because " << e.what() << std::endl;
	}
	catch (Form::NotSignedException & e) {
		std::cout << this->_name << " cannot execute " << form.getName() << " because " << e.what() << std::endl;
	}
}

std::ostream&	operator<<(std::ostream & out, Bureaucrat const & in) {

	out << in.getName() << ", bureaucrat grade " << in.getGrade() << std::endl;

	return out;
}

const char * Bureaucrat::GradeTooHighException::what() const throw() {

	return("The grade can't be higher than 1.");
}

const char * Bureaucrat::GradeTooLowException::what() const throw() {

	return("The grade can't be lower than 150.");
}

Bureaucrat::GradeTooHighException::GradeTooHighException(void) {}

Bureaucrat::GradeTooHighException::~GradeTooHighException(void) throw() {}

Bureaucrat::GradeTooLowException::GradeTooLowException(void) {}

Bureaucrat::GradeTooLowException::~GradeTooLowException(void) throw() {}
