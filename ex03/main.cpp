/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 10:24:16 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/14 18:18:56 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "Bureaucrat.class.hpp"
#include "Form.class.hpp"
#include <iostream>

int		main(void) {

	Intern		jack;
	Bureaucrat	jacky("Jacky", 1);
	Bureaucrat	jacko("Jacko", 42);
	Form		*tmp;


	tmp = jack.makeForm("shrubbery creation", "Forest");
	std::cout << "Try to execute it with a Bureaucrat 42:" << std::endl;
	jacky.signForm(*tmp);
	jacko.executeForm(*tmp);
	delete tmp;
	std::cout << std::endl;

	tmp = jack.makeForm("robotomy request", "Bender");
	std::cout << "Try to execute it with a Bureaucrat 42:" << std::endl;
	jacky.signForm(*tmp);
	jacko.executeForm(*tmp);
	delete tmp;
	std::cout << std::endl;

	tmp = jack.makeForm("presidential pardon", "Rambo");
	std::cout << "Try to execute it with a Bureaucrat 42:" << std::endl;
	jacky.signForm(*tmp);
	jacko.executeForm(*tmp);
	delete tmp;
	std::cout << std::endl;

	tmp = jack.makeForm("fe~JEfw#", "eowha£*H");

	return 0;
}
