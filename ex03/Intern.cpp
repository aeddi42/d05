/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 18:10:50 by plastic           #+#    #+#             */
/*   Updated: 2015/04/14 19:31:01 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

			Intern::Intern(void) {}

			Intern::~Intern(void) {}

Form*		Intern::makeForm(std::string const & form, std::string const & target) {

		Form*	ret;

		if (form == "shrubbery creation")
			ret = new ShrubberyCreationForm(target);
		else if (form == "robotomy request")
			ret = new RobotomyRequestForm(target);
		else if (form == "presidential pardon")
			ret = new PresidentialPardonForm(target);
		else {
			std::cout << "Error: Unknow form " << form << std::endl;
			return NULL;
		}
		std::cout << "Intern creates " << form << std::endl;
		return ret;
}
