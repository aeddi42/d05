/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 18:10:50 by plastic           #+#    #+#             */
/*   Updated: 2015/04/13 18:26:19 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
# define INTERN_HPP

# include "Form.class.hpp"

class Intern {

	public:
		Intern(void);
		~Intern(void);

		Form*	makeForm(std::string const & form, std::string const & target);

	private:
		Intern(Intern const & src);
		Intern& operator=(Intern const & rhs);

};

#endif /* !INTERN_HPP */
